package gitfox.model.data.state

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

internal class ServerChanges : CoroutineScope by CoroutineScope(Dispatchers.Main) {

    private val issueChannel = MutableStateFlow(0L)
    private val mergeRequestChannel = MutableStateFlow(0L)
    private val projectChannel = MutableStateFlow(0L)
    private val labelChannel = MutableStateFlow(0L)
    private val milestoneChannel = MutableStateFlow(0L)
    private val todoChannel = MutableStateFlow(0L)
    private val userChannel = MutableStateFlow(0L)
    private val memberChannel = MutableStateFlow(0L)

    val issueChanges: StateFlow<Long> = issueChannel
    val mergeRequestChanges: StateFlow<Long> = mergeRequestChannel
    val projectChanges: StateFlow<Long> = projectChannel
    val labelChanges: StateFlow<Long> = labelChannel
    val milestoneChanges: StateFlow<Long> = milestoneChannel
    val todoChanges: StateFlow<Long> = todoChannel
    val userChanges: StateFlow<Long> = userChannel
    val memberChanges: StateFlow<Long> = memberChannel

    fun issueChanged(id: Long = -1) {
        launch { issueChannel.value = id }
    }

    fun mergeRequestChanged(id: Long = -1) {
        launch { mergeRequestChannel.value = id }
    }

    fun projectChanged(id: Long = -1) {
        launch { projectChannel.value = id }
    }

    fun labelChanged(id: Long = -1) {
        launch { labelChannel.value = id }
    }

    fun milestoneChanged(id: Long = -1) {
        launch { milestoneChannel.value = id }
    }

    fun todoChanged(id: Long = -1) {
        launch { todoChannel.value = id }
    }

    fun userChanged(id: Long = -1) {
        launch { userChannel.value = id }
    }

    fun memberChanged(id: Long = -1) {
        launch { memberChannel.value = id }
    }
}
